<?php

//Generate a 4 digit code and unique uuid for an AtomJump notification server. Time decayed.
//Or get a uuid for a known 4 digit code
	
//$staging = true;		//Use when testing on staging only.

/*
Note: you must have this table in the database:

DROP TABLE IF EXISTS `tbl_notification_pairing`;
CREATE TABLE `tbl_notification_pairing` (
  `int_pairing_id` int(11) NOT NULL AUTO_INCREMENT,
  `var_guid` varchar(255) DEFAULT NULL,
  `var_passcode` varchar(10) DEFAULT NULL,
  `dt_set` datetime DEFAULT NULL,
  `dt_expires` datetime DEFAULT NULL,
  `var_proxy` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`int_pairing_id`),
  KEY `pass` (`var_passcode`),
  KEY `expires` (`dt_expires`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
*/

/**
 *  An example CORS-compliant method.  It will allow any GET, POST, or OPTIONS requests from any
 *  origin.
 *
 *  In a production environment, you probably want to be more restrictive, but this gives you
 *  the general idea of what is involved.  For the nitty-gritty low-down, read:
 *
 *  - https://developer.mozilla.org/en/HTTP_access_control
 *  - https://fetch.spec.whatwg.org/#http-cors-protocol
 *
 */
function cors() {

	// Allow from any origin
	if (isset($_SERVER['HTTP_ORIGIN'])) {
		// Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
		// you want to allow, and if so:
		header("Access-Control-Allow-Origin: *");		//Used to be: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
	} else {
		header("Access-Control-Allow-Origin: *");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
	}

	// Access-Control headers are received during OPTIONS requests
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
			// may also be using PUT, PATCH, HEAD etc
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		return;
	}
	return;		
}

function return_message($msg) {
	//If jsonp, wrap it in a JSON response, otherwise just display the message
	if(isset($_REQUEST['callback'])) {
		echo $_GET['callback'] . "(" . json_encode($msg) . ")";
	} else {
		echo $msg;
	}
}


function trim_trailing_slash_local($str) {
	return rtrim($str, "/");
}

function add_trailing_slash_local($str) {
	//Remove and then add
	return rtrim($str, "/") . '/';
}

if(!isset($notifications_config)) {
	//Get global plugin config - but only once
	$data = file_get_contents (dirname(__FILE__) . "/config/config.json");
	if($data) {
		$notifications_config = json_decode($data, true);
		if(!isset($notifications_config)) {
			return_message("Error: notifications config/config.json is not valid JSON.");
			exit(0);
		}
 
	} else {
		return_message("Error: Missing config/config.json in notifications plugin.");
		exit(0);
 
	}

}



$start_path = add_trailing_slash_local($notifications_config['serverPath']);

$staging = $notifications_config['staging'];
$notify = false;
include_once($start_path . 'config/db_connect.php');	

function preserve_qs() {
    if (empty($_SERVER['QUERY_STRING']) && strpos($_SERVER['REQUEST_URI'], "?") === false) {
        return "";
    }
    return "?" . $_SERVER['QUERY_STRING'];
}

function get_least_load($server_pool, $country_code) {
	//Input 1: an array of country codes, with an array of servers on each one
	//      2: a country code
	//
	//Check if there is an output file from check-load.php 'outgoings/load.json', and if so
	//    use the least loaded server
	//If not load checks have been done, select from the array list for this country at random
	
	//Returns the preferred URL of the proxy MedImage server
	
	$load_file = __DIR__ . "/outgoing/load.json";
	if(file_exists($load_file)) {
		$load_file_str = file_get_contents($load_file);
		$load = json_decode($load_file_str, true);
		if(isset($load)) {
			$server_sorted_pool = $load['atomjumpNotifications']['serverPoolLoad'][$country_code];
			if((isset($server_sorted_pool)) &&
				(isset($server_sorted_pool[0]['url'])) ) {
				if(!$server_sorted_pool[0]['load']) {
					error_log("Warning: Sorry, the country code " . $country_code . " does not have any responding notification servers, using the first option listed, regardless.");
				}
				return $server_sorted_pool[0]['url'];			
			} else {
				error_log("Warning: Sorry, the country code " . $country_code . " file is not correctly in the load file, using a random selection instead.");
			}
		} else {
			error_log("Warning: Sorry, the " . $load_file . " file is not correct JSON, using a random selection instead.");
		}
	} 
	
	//Falling through, use a random selection from the config file
	$random_top = count($server_pool);
	
	$selected = rand(1, $random_top);
	return $server_pool[$selected-1];

}


function expand_numbers($server_pool_arr)
{
	//Inputs: an array of servers:
	/*
			['https://your.medimageserver.nz.url1:5566',
			'https://your.medimageserver.nz.url2:5566',
			'https://your.medimageserver.nz.url[3-20]:5566',
			'https://your.medimageserver.nz.url21:5566']
	
	*/
	//Outputs:
	//An array with the numbers expanded into individual entries e.g. in this case
	/* ['https://your.medimageserver.nz.url1:5566', 
	    'https://your.medimageserver.nz.url2:5566',
	    'https://your.medimageserver.nz.url3:5566',
	    'https://your.medimageserver.nz.url4:5566',
	    'https://your.medimageserver.nz.url5:5566',
	    			...
	    'https://your.medimageserver.nz.url20:5566',
	    'https://your.medimageserver.nz.url21:5566']
	*/
	//So the [3-20] is expanded into individual entries.
	//The array is then returned with it's expanded entries
	$verbose = false;
	$entry = 0;
	$output_array = array();
	
	for($cnt = 0; $cnt < count($server_pool_arr); $cnt++) {
	
		$detected_nums = false;
		$start_at = strpos($server_pool_arr[$cnt], '[');
		if($start_at !== false) {
			//Likely found a number to expand
			$end_at = strpos($server_pool_arr[$cnt], ']');
			if($end_at !== false) {
				//Definitely found a number to expand
				$detected_nums = true;
				
				$isolated = substr($server_pool_arr[$cnt], ($start_at+1), ($end_at - ($start_at+1)));
				if($verbose == true) echo "Detected: " . $isolated . "\n";
				$numbers = explode("-", $isolated);
				
				$from = $numbers[0];
				$to = $numbers[1];
				if($verbose == true) echo "From: " . $from . "  To: " . $to . "\n";
				
				$before_str = substr($server_pool_arr[$cnt], 0, $start_at);
				$after_str = substr($server_pool_arr[$cnt], $end_at+1);
				
				if($verbose == true) echo "Before str: " . $before_str . "  After str: " . $after_str . "\n";
				for($n = $from; $n <= $to; $n++) {
					$val = $before_str . $n . $after_str;
					$output_array[$entry] = $val;	
					$entry++;
				}
			}
		}
		
		if($detected_nums == false) {
			//Insert a regular entry into the array
			$output_array[$entry] = $server_pool_arr[$cnt];	
			$entry++;
		}
	}
	
	return $output_array;
	
}



 $decay = 2; //in hours
 
 if(isset($_GET['proxyServer'])) {
   $proxy = $_GET['proxyServer'];		//the urldecoding potentially differs from the REQUEST version.
   $country_used = rawurlencode("[Unknown - private]");
 
 
 } elseif(isset($_REQUEST['proxyServer'])) {
   $proxy = $_REQUEST['proxyServer'];
   $country_used = rawurlencode("[Unknown - private]");
 } else {
   //Use aj default proxy, based on country
   
   //Defaults
   $proxy = "https://medimage-wrld.atomjump.com";
   $country_used = rawurlencode("New Zealand");
   
   if(isset($_REQUEST['country'])) {
   	$country_code = $_REQUEST['country'];
   	
   	if(isset($notifications_config['atomjumpNotifications']) && isset($notifications_config['atomjumpNotifications']['serverPool'])) {
   		if(isset($notifications_config['atomjumpNotifications']['serverPool'][$country_code])) {
   			//Select the 1st option in the country. choose the least load option
   			$proxy = get_least_load(expand_numbers($notifications_config['atomjumpNotifications']['serverPool'][$country_code]), $country_code);
   		} else {
   			if(isset($notifications_config['atomjumpNotifications']['serverPool']['Default'])) {
   				$proxy = get_least_load(expand_numbers($notifications_config['atomjumpNotifications']['serverPool']['Default']), 'Default');
   			} else {
   				return_message("noproxy");
   			}
   		}
   	}
   	
   	if(isset($notifications_config['atomjumpNotifications']) && isset($notifications_config['atomjumpNotifications']['countryServerResidingIn'])) {
   		
   		if(isset($notifications_config['atomjumpNotifications']['countryServerResidingIn'][$country_code])) {
   			$country_used = rawurlencode($notifications_config['atomjumpNotifications']['countryServerResidingIn'][$country_code]);		//raw so that spaces are not encoded as '+'
   		} else {
   			if(isset($notifications_config['atomjumpNotifications']['countryServerResidingIn']['Default'])) {
   				$country_used = rawurlencode($notifications_config['atomjumpNotifications']['countryServerResidingIn']['Default']);
   			} else {
   				$country_used = rawurlencode("[Unknown - private]");
   			}
   		}
   	}
   		
   }
   
 }

 if(isset($_REQUEST['compare'])) {
 
     $sql = "SELECT * FROM tbl_notification_pairing WHERE var_passcode = ? AND dt_expires > NOW()";
     $result = dbquery($sql, [["s",$_REQUEST['compare']]] )  or die("Unable to execute query $sql " . dberror());
     if($row = db_fetch_array($result))
	    {
	      	   $guid = $row['var_guid'];
	      	   $proxy = $row['var_proxy'];
	      	   echo $proxy . '/write/' . $guid; 	   
	    } else {
	         return_message('nomatch');
	    }

 
 } else {
    //Add a new code
    
    $letters = "23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";
    $letters_max = strlen($letters) - 1 ;
    
    
    //Loop until find a random unique 4 digit code
    // still active
    //Check code doesn't already exist
     
    do {  
    
       $passcode = "";
       
       for($cnt = 0; $cnt< 4; $cnt++) {
         $passcode .= $letters[rand(0, $letters_max)];
       }
       //Old way: $passcode = rand(1000, 9999);
       $found = null;
 
       $sql = "SELECT * FROM tbl_notification_pairing WHERE var_passcode = ? AND dt_expires > NOW()";
  		$result = dbquery($sql, [["s", $passcode]])  or die("Unable to execute query $sql " . dberror());
	      if($row = db_fetch_array($result))
      	{
      	   $found = $row['int_pairing_id'];
      	}
    
    } while($found != null); //keep looping until found a unique option
    
    
    //Generate a guid
    if($_REQUEST['guid']) {
    	//If we already have a GUID on the server, just generate a new passcode, and leave the same GUID
    	$guid = $_REQUEST['guid'];
    	
    	//Check if GUID exists in the database (only situation where this wouldn't be here would be if we have lost the old database), and this is 
    	//an exising MedImage server
    	$guid_exists = false;
    	$sql = "SELECT * FROM tbl_notification_pairing WHERE var_guid = ?";
    	$result = dbquery($sql, [['s',$guid]])  or die("Unable to execute query $sql " . dberror());
    	if($row = db_fetch_array($result))
	   {
	      	   //Found the GUID
	      	   $guid_exists = true;
	      	   if($row['var_proxy']) {
	      	   		//Keep the same var_proxy
	      	   	   $proxy = $row['var_proxy'];
	      	   }
	    }
    	
    	if($guid_exists == true) {
    		//Update the old record
    		$sql = "UPDATE tbl_notification_pairing SET var_passcode = ?, dt_set = NOW(), dt_expires = DATE_ADD(NOW(), INTERVAL ? HOUR), 			var_proxy = ? WHERE var_guid = ?"; 
    		cors();
    	 	$result = dbquery($sql, [["s",$passcode],["i", $decay],["s", $proxy],["s", $guid]] )  or die("Unable to execute query $sql " . dberror());
    	} else {
    		//GUID does not exist in this database, but we'll assume it is valid data.
            $sql = "INSERT INTO tbl_notification_pairing ( var_guid, var_passcode, dt_set, dt_expires, var_proxy) VALUES ( ?, ?, NOW(), DATE_ADD(NOW(), INTERVAL ? HOUR), ?);";
    		cors();
    	 	$result = dbquery($sql, [["s", $guid],["s", $passcode], ["i",$decay], ["s",$proxy]] )  or die("Unable to execute query $sql " . dberror());
    	}
    	
    	
    	
    } else {
 		//Generate a new GUID
    	$guid_exists = true;
 		
 		//Loop until we have a unique GUID
    	while($guid_exists == true) {
    	
			$guid = substr(str_shuffle(str_repeat('23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ', mt_rand(1,20))),1,20);	//Note: 20 is slightly longer than 18 letters in MedImage pairing. Therefore will always be differentiated.
			
			//Check if GUID exists in the database (only situation where this wouldn't be here would be if we have lost the old database), and this is 
			//an exising MedImage server
			$guid_exists = false;
			$sql = "SELECT * FROM tbl_notification_pairing WHERE var_guid = ?";
			$result = dbquery($sql, [["s", $guid]] )  or die("Unable to execute query $sql " . dberror());
			if($row = db_fetch_array($result))
			{
				   //Found the GUID
				   $guid_exists = true;
			   
			}
		}
    	
    	//Insert the code and guid
         $sql = "INSERT INTO tbl_notification_pairing (var_guid, var_passcode, dt_set, dt_expires, var_proxy) VALUES ( ?, ?, NOW(), DATE_ADD(NOW(), INTERVAL ? HOUR), ?);";
         cors();
    	 $result = dbquery($sql, [["s", $guid],["s",$passcode],["i", $decay],["s", $proxy]] )  or die("Unable to execute query $sql " . dberror());

    }


    return_message($passcode . " " . $guid . " " . $proxy . " " . $country_used);
 }

?>
